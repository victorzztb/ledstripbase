<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Clipper">
<description>Connector Host for NEOPIXEL</description>
<packages>
<package name="NEOSTRIPCONN254">
<description>Dimension Reference 2.54 ( for strip )</description>
<smd name="P2" x="0" y="0" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<smd name="P3" x="0" y="2.54" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<smd name="P1" x="0" y="-2.54" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<wire x1="-1.27" y1="3.81" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="-1.27" y2="-3.81" width="0.127" layer="21"/>
<text x="-1.27" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="NEOSTRIPCONNFLEX">
<description>The flexible connector (also supports 144 led, but less "safe")</description>
<smd name="P2" x="0" y="0" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<smd name="P3" x="0" y="2.54" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<smd name="P1" x="0" y="-2.54" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="0.635" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="0.635" y2="-5.08" width="0.127" layer="21"/>
<polygon width="0.0762" layer="1">
<vertex x="0" y="2.54"/>
<vertex x="-1.905" y="2.54"/>
<vertex x="-1.905" y="4.7625"/>
<vertex x="-0.3175" y="4.7625"/>
<vertex x="0" y="4.445"/>
</polygon>
<polygon width="0.0762" layer="1">
<vertex x="-1.905" y="-2.54"/>
<vertex x="-1.905" y="-4.7625"/>
<vertex x="-0.3175" y="-4.7625"/>
<vertex x="0" y="-4.445"/>
<vertex x="0" y="-2.54"/>
</polygon>
<text x="-1.905" y="5.3975" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-6.6675" size="1.27" layer="27">&gt;VALUE</text>
<polygon width="0.2032" layer="29">
<vertex x="-1.905" y="2.54"/>
<vertex x="-1.905" y="4.7625"/>
<vertex x="-0.3175" y="4.7625"/>
<vertex x="0" y="4.445"/>
<vertex x="0" y="2.54"/>
</polygon>
<polygon width="0.2032" layer="29">
<vertex x="-1.905" y="-2.54"/>
<vertex x="-1.905" y="-4.7625"/>
<vertex x="-0.3175" y="-4.7625"/>
<vertex x="0" y="-4.445"/>
<vertex x="0" y="-2.54"/>
</polygon>
<polygon width="0.2032" layer="31">
<vertex x="-1.905" y="2.54"/>
<vertex x="-1.905" y="4.7625"/>
<vertex x="-0.3175" y="4.7625"/>
<vertex x="0" y="4.445"/>
<vertex x="0" y="2.54"/>
</polygon>
<polygon width="0.2032" layer="31">
<vertex x="-1.905" y="-2.54"/>
<vertex x="-1.905" y="-4.7625"/>
<vertex x="-0.3175" y="-4.7625"/>
<vertex x="0" y="-4.445"/>
<vertex x="0" y="-2.54"/>
</polygon>
</package>
<package name="JR254">
<description>GVS JR Futaba</description>
<smd name="V" x="0" y="0" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<smd name="S" x="0" y="2.54" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
<smd name="G" x="0" y="-2.54" dx="3.81" dy="1.9304" layer="1" roundness="100"/>
</package>
</packages>
<symbols>
<symbol name="NEOPIXELCONNDO">
<pin name="DO" x="0" y="0" length="middle"/>
<pin name="GND" x="0" y="5.08" length="middle" direction="pwr"/>
<pin name="5V" x="0" y="-5.08" length="middle" direction="pwr"/>
<wire x1="5.08" y1="5.08" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
</symbol>
<symbol name="JR254">
<description>GVS JR/Futaba</description>
<pin name="V" x="0" y="0" length="middle" direction="pwr"/>
<pin name="S" x="0" y="5.08" length="middle"/>
<pin name="G" x="0" y="-5.08" length="middle" direction="pwr"/>
<wire x1="5.08" y1="5.08" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="NEOPIXELCONNDO">
<description>NEOPIXELDINPCB
Connects to DO</description>
<gates>
<gate name="G$1" symbol="NEOPIXELCONNDO" x="0" y="0"/>
</gates>
<devices>
<device name="STANDARD" package="NEOSTRIPCONN254">
<connects>
<connect gate="G$1" pin="5V" pad="P1"/>
<connect gate="G$1" pin="DO" pad="P2"/>
<connect gate="G$1" pin="GND" pad="P3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERSITILE" package="NEOSTRIPCONNFLEX">
<connects>
<connect gate="G$1" pin="5V" pad="P1"/>
<connect gate="G$1" pin="DO" pad="P2"/>
<connect gate="G$1" pin="GND" pad="P3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JR254">
<description>Laying down version of JR connector ( Single sided )</description>
<gates>
<gate name="G$1" symbol="JR254" x="0" y="0"/>
</gates>
<devices>
<device name="PAD254" package="JR254">
<connects>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
<connect gate="G$1" pin="V" pad="V"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="Clipper" deviceset="NEOPIXELCONNDO" device="VERSITILE"/>
<part name="U$2" library="Clipper" deviceset="JR254" device="PAD254"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="10.16" y="33.02" smashed="yes"/>
<instance part="U$2" gate="G$1" x="-20.32" y="33.02" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="DO" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="S"/>
<wire x1="-20.32" y1="38.1" x2="-27.94" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="38.1" x2="-27.94" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="48.26" x2="5.08" y2="48.26" width="0.1524" layer="91"/>
<wire x1="5.08" y1="48.26" x2="5.08" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="DO"/>
<wire x1="5.08" y1="33.02" x2="10.16" y2="33.02" width="0.1524" layer="91"/>
<label x="-25.4" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="5V"/>
<wire x1="10.16" y1="27.94" x2="2.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="2.54" y1="27.94" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="2.54" y1="30.48" x2="-27.94" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="30.48" x2="-27.94" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="V"/>
<wire x1="-27.94" y1="33.02" x2="-20.32" y2="33.02" width="0.1524" layer="91"/>
<label x="-25.4" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="G"/>
<wire x1="-20.32" y1="27.94" x2="-27.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="27.94" x2="-27.94" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="22.86" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
<wire x1="27.94" y1="22.86" x2="27.94" y2="48.26" width="0.1524" layer="91"/>
<wire x1="27.94" y1="48.26" x2="7.62" y2="48.26" width="0.1524" layer="91"/>
<wire x1="7.62" y1="48.26" x2="7.62" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="7.62" y1="38.1" x2="10.16" y2="38.1" width="0.1524" layer="91"/>
<label x="-25.4" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
