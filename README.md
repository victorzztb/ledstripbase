# LEDStripBase
####CC-BY

*The common "neopixel" strips arranged power/gnd on the side and signal in the middle, which will be risky for accidental reverse connection. This board reverses 5V and Signal pin to make it compatible with most servo cables. The pcb has 2 parts: one for DI and one for DO.*

* This is a base board for more robust/versitle connection with
    * 1st version is with 2.54 connectors
    * Use the 2.54 (Solder in SMD fashion for connection) for versitle prototype
    * The slots are for securing if you want to directly solder wires

* For the 2.54 Version
    * Can't export the milling layer
    * Dimension layer added for the outline for fabrication
    * You can use the original file to export again as EAGLE has hobbiest license.